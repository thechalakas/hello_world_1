package com.thechalakas.jay.hello_world_1;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static android.R.id.message;
import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity
{

    /*
             final Button button = (Button) findViewById(R.id.button_id);
         button.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                 // Perform action on click
             }
         });
     */





    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //first up, I am going to make the button change the text.

        //let me get the button object on the UI, here.
        Button button_for_hello = (Button) findViewById(R.id.button_tap);

        //same as above, this one to change color
        Button button_for_color = (Button) findViewById(R.id.button_tap_red);

        //this is for the reset of the view.
        Button button_for_reset = (Button) findViewById(R.id.button_reset);

        //this is to go the next activity
        Button button_for_next = (Button) findViewById(R.id.button_next);

        //I want the default color to be red here.
        TextView textview_code_hello = (TextView) findViewById(R.id.textview_ui_hello);
        textview_code_hello.setTextColor(0xffff0000);
        textview_code_hello.setTextSize(25.0f);


        //listener for the button tap
        button_for_hello.setOnClickListener(new View.OnClickListener()
        {

            //setting the listener (which listens for taps) and make it do what I want it to do
            //in this case, change the text to something else.

            @Override
            public void onClick(View v)
            {
                TextView textview_code_hello = (TextView) findViewById(R.id.textview_ui_hello);
                String string_to_change = "You just tapped this! Wow, you are such a genius man";
                textview_code_hello.setText(string_to_change);
            }
        });


        //listener for button red
        button_for_color.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                TextView textview_code_hello = (TextView) findViewById(R.id.textview_ui_hello);
                //setting this to yellow here.
                textview_code_hello.setTextColor(Color.GREEN);
            }
        });

        button_for_reset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                TextView textview_code_hello = (TextView) findViewById(R.id.textview_ui_hello);

                textview_code_hello.setTextColor(0xffff0000);  //taking the text back to red
                String text_to_show = "Hello! Tap to Change this";
                textview_code_hello.setText(text_to_show);
            }
        });

        button_for_next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*
                        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
                 */

                //this intent will allow activity navigation
                //first pass the context, and then, the activity to pass
                //normally, I could just do 'this' for the context. However, sometimes, the
                // current method does not have access to the context
                //i which case, getApplicationContext works just fine.
                Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                startActivity(intent);
            }
        });
    }
}
