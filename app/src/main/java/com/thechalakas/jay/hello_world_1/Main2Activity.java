package com.thechalakas.jay.hello_world_1;

import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Console;

public class Main2Activity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //the button that will make the web call and also show the results in the text box.
        Button button_get_http_stuff = (Button) findViewById(R.id.button_get);

        //let me get the progress bar object.
        //but before that, let me commit this.
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //setting it to just spin indefinitely
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.INVISIBLE);

        //the text view object
        final TextView textview_second_activity = (TextView) findViewById(R.id.textview_ui_second_activity);

        button_get_http_stuff.setOnClickListener(new View.OnClickListener()
        {



            @Override
            public void onClick(View v)
            {
                Log.d("tag1","button clicked");
                //alright lets do this web request thing.
                //thank god this volley thing is here man. the old android web stuff was weird.
                //may be this is beter.

                /*
// Instantiate the RequestQueue.
RequestQueue queue = Volley.newRequestQueue(this);
String url ="http://www.google.com";

// Request a string response from the provided URL.
StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
            new Response.Listener<String>() {
    @Override
    public void onResponse(String response) {
        // Display the first 500 characters of the response string.
        mTextView.setText("Response is: "+ response.substring(0,500));
    }
}, new Response.ErrorListener() {
    @Override
    public void onErrorResponse(VolleyError error) {
        mTextView.setText("That didn't work!");
    }
});
// Add the request to the RequestQueue.
queue.add(stringRequest);
                 */

                //There are HTTP libraries available to make it easy to make web calls.
                //I for one am extremely happy that these libraries are now available.
                //I mean, I remember having to jump through a lot of loops just to get web calls in android earlier.

                //Other than Volley, there are other libraries (for example retrofit) that can do the same.
                //I am going with Volley simply because, it is developed by Google. Advanced developers can of course, try alternatives
                //me though, I will stick with volley

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());  //this is kind of like the web call object.
                String url = "http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/";  //yep, I use bing. nothing against google. Just like bing.

                progressBar.setVisibility(View.VISIBLE); //progress bar becomes visible. funnily enough, it is always spinning :P


                //this is where the execution happens, and we collect the results - good or bad.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>()
                {
                    //aha, this is where the actual response comes after execution. Inside is also when we must make the
                    //progress wheel disappear.
                    @Override
                    public void onResponse(String response)
                    {
                        textview_second_activity.setText((response));
                        Log.d("tag1","button clicked response");

                        try //we are here means, things went fine.
                        {
                            //JSONObject jsonObject = new JSONObject(response);
                            //I am collecting the response into a JSON Array
                            JSONArray jsonArray = new JSONArray(response);
                            Log.d("tag1","json conversion worked fine");
                            progressBar.setVisibility(View.INVISIBLE);

                        }
                        catch (JSONException e) //man, something went bad dude.
                        {
                            Log.d("tag1","json conversion went bad");
                            Log.d("tag1",e.toString());
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        progressBar.setVisibility(View.INVISIBLE);
                        textview_second_activity.setText("something went wrong man!");
                        Log.d("tag1","button error");
                    }
                }
                );

                queue.add(stringRequest);  //this is where the actual web call is triggered.
            }
        });
    }
}
